3ch3r46/bootui-breadcrumb
=========================
The Bootstrap Triangle Breadcrumbs Arrows for yii framework.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist 3ch3r46/bootui-breadcrumb "*"
```

or add

```
"3ch3r46/bootui-breadcrumb": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code, replace Breadcrumb default in layout/main with code below :

~~~
<?php
use bootui\breadcrumb\Breadcrumb;

echo Breadcrumb::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
~~~

Property Type and Size
----------------------
Type :

- `default` or `Breadcrumb::TYPE_DEFAULT`.

- `primary` or `Breadcrumb::TYPE_PRIMARY`.

- `success` or `Breadcrumb::TYPE_SUCCESS`.

- `info` or `Breadcrumb::TYPE_INFO`.

- `warning` or `Breadcrumb::TYPE_WARNING`.

- `danger` or `Breadcrumb::TYPE_DANGER`.

Size :

- `sm` or `Breadcrumb::SMALL`.

- `lg` or `Breadcrumb::LARGE`.

example:

~~~
<?php
use bootui\breadcrumb\Breadcrumb;

echo Breadcrumb::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    'type' => Breadcrumb::TYPE_PRIMARY,
    'size' => Breadcrumb::LARGE,
]);
~~~
