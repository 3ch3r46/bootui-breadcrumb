<?php
namespace bootui\breadcrumb;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * Bootstrap Triangle Breadcrumb Arrows.
 * Replace Breadcrumb default in layout/main with code below:
 * ~~~
 * [php]
 * Breadcrumb::widget([
 *     'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
 * ]);
 * ~~~
 * 
 * Property Type and Size
 * Type : 
 * - `default` or `Breadcrumb::TYPE_DEFAULT`.
 * - `primary` or `Breadcrumb::TYPE_PRIMARY`.
 * - `success` or `Breadcrumb::TYPE_SUCCESS`.
 * - `info` or `Breadcrumb::TYPE_INFO`.
 * - `warning` or `Breadcrumb::TYPE_WARNING`.
 * - `danger` or `Breadcrumb::TYPE_DANGER`.
 * Size :
 * - `sm` or `Breadcrumb::SMALL`.
 * - `lg` or `Breadcrumb::LARGE`.
 * 
 * example:
 * ~~~
 * [php]
 * Breadcrumb::widget([
 *     'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
 *     'type' => Breadcrumb::TYPE_PRIMARY,
 *     'size' => Breadcrumb::LARGE,
 * ]);
 * ~~~
 * 
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class Breadcrumb extends \yii\widgets\Breadcrumbs
{
	const TYPE_DEFAULT = 'default';
	const TYPE_PRIMARY = 'primary';
	const TYPE_INFO = 'info';
	const TYPE_SUCCESS = 'success';
	const TYPE_WARNING = 'warning';
	const TYPE_DANGER = 'danger';
	
	const SMALL = 'sm';
	const LARGE = 'lg';

	public $tag = 'div';
	
	public $options = ['class' => 'btn-breadcrumb btn-group'];
	
	public $itemTemplate = "{link}\n";
	
	public $activeItemTemplate = "{link}\n";
	
	public $type = 'default';
	
	public $size;
	
	public $fullWidth = true;
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\base\Object::init()
	 */
	public function init()
	{
		parent::init();
		if (isset($this->size) && in_array($this->size, ['sm','lg'])) {
			Html::addCssClass($this->options, 'btn-group-'.$this->size);
		}
		if (in_array($this->type, ['default', 'primary', 'success', 'info', 'warning', 'danger'])) {
			Html::addCssClass($this->options, 'breadcrumb-' . $this->type);
		}
		if ($this->fullWidth) {
			Html::addCssClass($this->options, 'btn-breadcrumb-full');
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \yii\widgets\Breadcrumbs::run()
	 */
    public function run()
    {
    	BreadcrumbPlugin::register($this->getView());
        if (empty($this->links)) {
            return;
        }
        $links = [];

        $linkOptions = $this->preprareLinkOptions();
        if ($this->homeLink === null) {
            $links[] = $this->renderItem([
                'label' => Yii::t('yii', 'Home'),
                'url' => Yii::$app->homeUrl,
            	'options' => $linkOptions,
            ], $this->itemTemplate);
        } elseif ($this->homeLink !== false) {
        	$this->homeLink = ArrayHelper::merge($this->homeLink, ['options' => $linkOptions]);
            $links[] = $this->renderItem($this->homeLink, $this->itemTemplate);
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            if (isset($link['options'])) {
            	if (isset($link['options']['class']))
            		Html::addCssClass($linkOptions, ArrayHelper::remove($link['options'], 'class'));
            	$link['options'] = ArrayHelper::merge($link['options'], $linkOptions);
            } else {
            	$link['options'] = $linkOptions;
            }
            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }
        return Html::tag($this->tag, implode('', $links), $this->options);
    }
    
    protected function preprareLinkOptions()
    {
    	$options = ['class' => 'btn'];
    	
    	if (in_array($this->type, ['default', 'primary', 'success', 'info', 'warning', 'danger'])) {
    		Html::addCssClass($options, 'btn-' . $this->type);
    	}
    	
    	return $options;
    }
    
    /**
     * Renders a single breadcrumb item.
     * @param array $link the link to be rendered. It must contain the "label" element. The "url" element is optional.
     * @param string $template the template to be used to rendered the link. The token "{link}" will be replaced by the link.
     * @return string the rendering result
     * @throws InvalidConfigException if `$link` does not have "label" element.
     */
    protected function renderItem($link, $template)
    {
    	if (isset($link['label'])) {
    		$label = $this->encodeLabels ? Html::encode($link['label']) : $link['label'];
    	} else {
    		throw new InvalidConfigException('The "label" element is required for each link.');
    	}
    	if (isset($link['url'])) {
    		return strtr($template, ['{link}' => Html::a($label, $link['url'], $link['options'])]);
    	} else {
    		Html::addCssClass($link['options'], 'active');
    		return strtr($template, ['{link}' => Html::a($label, null, $link['options'])]);
    	}
    }
}
