<?php
namespace bootui\breadcrumb;

use yii\web\AssetBundle;
/**
 * Bootstrap Asset Bundle
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class BreadcrumbPlugin extends AssetBundle
{
    public $sourcePath = '@bootui/breadcrumb/css';
    
    public $css = [
    	'breadcrumb.css',
    ];
    
    public $depends = [
    	'bootui\asset\CoreCss',
    ];
}